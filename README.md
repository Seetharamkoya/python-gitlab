# Python-gitlab to monitor the flask project
### How to run this app locally on Docker
* Make sure Docker is installed 
* Run command: `docker-compose up`
* Note that if you make changes to `Dockerfile` or `requirements.txt`, you will need to run command: `docker-compose build` to rebuild the image.
* After running the app, it should be located at `http://localhost/` (port 80). Note this is an override of the default port 5000 for Flask apps.

### Unit and Integration Tests 
Pytest is used to run unit and integ tests for this app. To run pytest from a locally, run cmd: `python3 -m pytest`

### CI/CD Pipeline 
The .gitlab-ci.yml file is where we define the stages and jobs for each stage to due test + build + Monitor processes. 

# Python-gitlab monitoring

### connect to gitlab instance
gl = gitlab.Gitlab(url='https://gitlab.example.com', private_token='$PERSONAL_ACCESS_TOKEN')
### connect to instance through cli
python gitlab_api.py --project-id < project ID> --job-id <job ID>

