import os
import gitlab

# Connect to the GitLab instance using the GitLab API.
gl = gitlab.Gitlab('https://gitlab.com/', private_token=os.getenv('PERSONAL_ACCESS_TOKEN'))


# Fetch the status of jobs in a specified GitLab project.
def monitor_jobs(project_id):
    project = gl.projects.get(project_id)
    pipelines = project.pipelines.list()
    jobs = project.jobs.list()
    for job in jobs:
        print(f"Job: {job.name} | Status: {job.status}")
    monitor_jobs(project_id)


#Fetch the job logs for the specified GitLab project, pipeline, and job.
def fetch_job_logs(project_id, job_id):
    project = gl.projects.get(project_id)
    job = project.jobs.get(job_id)
    return job.trace()

# Analyze job logs for common issues
def analyze_logs(job_logs):
    # Search for patterns or keywords indicating common issues
    if "error" in job_logs:
        return "Error found in job logs"
    if "failed" in job_logs:
        return "Job failed"
    if "flaky" in job_logs:
        return "Flaky test detected"
    if "timeout" in job_logs:
        return "Job timed out"
    return None

# Generate early alerts with relevant information
def generate_alert(issue, job_name, log_entries):
    alert = f"Issue: {issue}\nAffected Job: {job_name}\nLog Entries:\n"
    alert += "\n".join(log_entries)
    return alert

# Post an alert as a new issue in the GitLab project
def post_alert_as_issue(project_id, issue, job_name, log_entries):
    project = gl.projects.get(project_id)
    alert = generate_alert(issue, job_name, log_entries)
    project.issues.create({'title': 'Job Alert', 'description': alert})
    
    
# Trigger a new pipeline in the GitLab project
def trigger_pipeline(project_id):
    project = gl.projects.get(project_id)
    project.pipelines.create({'ref': 'main'})